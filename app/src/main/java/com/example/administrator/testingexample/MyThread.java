package com.example.administrator.testingexample;

/**
 * Created by Administrator on 2016/10/20.
 */

class MyThread extends Thread{

    private String name = "";

    MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        super.run();
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.out.println(this.name + "卖票--->" + i);
            }
        }
    }
}
