package com.example.administrator.testingexample;

/**
 * 当两个并发线程访问同一个对象object中的这个synchronized(this)同步代码块时，
 * 一个时间内只能有一个线程得到执行。另一个线程必须等待当前线程执行完这个代码
 * 块以后才能执行该代码块。
 * Created by Administrator on 2016/10/21.
 */

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        synchronized (this) {
            for (int i = 1; i <= 10; i++) {
                System.out.println(Thread.currentThread().getName() +
                        " is sailing ticket " + i);
                try {
                    Thread.sleep(100);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
