package com.example.administrator.testingexample;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2016/10/21.
 */
public class MyThread02Test {

    private MyThread02 myThread02 = null;

    @Before
    public void setUp() throws Exception {
        myThread02 = new MyThread02();
    }

    @Test
    public void run() throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                myThread02.method01();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                myThread02.method02();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                myThread02.method03();
            }
        }).start();
        try {
            Thread.sleep(1000);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}