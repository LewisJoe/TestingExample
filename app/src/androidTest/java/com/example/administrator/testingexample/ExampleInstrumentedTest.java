package com.example.administrator.testingexample;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static final String STRING_TO_BE_TYPE = "Peter";
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.administrator.testingexample", appContext.getPackageName());
    }
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Test
    public void sayHello() {
        //-- 找到ID为editText的view，执行输入"Peter",然后关闭键盘
        onView(withId(R.id.editText)).perform(typeText(STRING_TO_BE_TYPE), closeSoftKeyboard());
        //-- 找到文字为Say hello的按钮，执行点击
        onView(withText("Say hello")).perform(click());
        String expectedText = "Hello," + STRING_TO_BE_TYPE + "!";
        //-- 将textView上的文字与预期的结果进行对比
        onView(withId(R.id.textView)).check(matches(withText(expectedText)));
    }
}
