package com.example.administrator.testingexample;

import org.junit.Before;
import org.junit.Test;

/**
 * Runnable测试
 * Created by Administrator on 2016/10/21.
 */
public class MyRunnableTest {

    private MyRunnable myRunnable = null;

    @Before
    public void setUp() throws Exception {
        myRunnable = new MyRunnable();
    }

    @Test
    public void Run() throws Exception {
        System.out.println("Welcome to Yannis.Chou Blog! \n"
                +"synchronized 关键字使用 \n"
                +"--------------------------");
        Thread t1 = new Thread(myRunnable, "窗口1");
        Thread t2 = new Thread(myRunnable, "窗口2");
        Thread t3 = new Thread(myRunnable, "窗口3");
        t1.start();
        t2.start();
        t3.start();
        try {
            Thread.sleep(3000);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}