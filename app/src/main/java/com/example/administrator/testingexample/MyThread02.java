package com.example.administrator.testingexample;

/**
 * 当一个线程访问object的一个synchronized(this)同步代码块时，
 * 另一个线程仍然可以访问该object中的非synchronized(this)同步代码块。
 * Created by Administrator on 2016/10/21.
 */

public class MyThread02 {

    public void method01() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 当一个线程访问object的一个synchronized(this)同步代码块时，
     * 它就获得了这个object的对象锁。
     * 结果，其它线程对该object对象所有同步代码部分的访问都被暂时阻塞。
     */
    public synchronized void method02() {
        for (int i = 0; i < 6; i++) {
            System.out.println(Thread.currentThread().getName() + ":" + i);
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 当一个线程访问object的一个synchronized(this)同步代码块时，
     * 另一个线程仍然可以访问该object中的非synchronized(this)同步代码块。
     */
    public void method03() {
        for (int i = 0; i < 2; i++) {
            System.out.println(Thread.currentThread().getName() + ":" + i);
        }
    }
}
