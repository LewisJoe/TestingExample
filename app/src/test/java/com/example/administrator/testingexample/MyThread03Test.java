package com.example.administrator.testingexample;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2016/10/21.
 */
public class MyThread03Test {

    private MyThread03 myThread03 = null;
    private MyThread03.InnerObject innerObject = null;

    @Before
    public void setUp() throws Exception {
        myThread03 = new MyThread03();
        innerObject = myThread03.new InnerObject();
    }

    @Test
    public void run() {
        System.out.println("Welcome to this test....\n" +
                "synchronized 关键字使用...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                myThread03.otherMethod01(innerObject);
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                myThread03.otherMethod02(innerObject);
            }
        }).start();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}