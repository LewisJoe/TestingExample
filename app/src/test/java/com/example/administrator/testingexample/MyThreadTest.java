package com.example.administrator.testingexample;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2016/10/20.
 */
public class MyThreadTest {
    private MyThread t1;
    private MyThread t2;
    private MyThread t3;
    @Before
    public void setUp() throws Exception {
        t1 = new MyThread("窗口1");
        t2 = new MyThread("窗口2");
        t3 = new MyThread("窗口3");
    }

    @Test
    public void Run() throws Exception {
        t1.start();
        t2.start();
        t3.start();
        try {
            Thread.sleep(1000);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}