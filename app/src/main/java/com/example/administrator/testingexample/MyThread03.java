package com.example.administrator.testingexample;

/**
 * synchronized对象锁
 * Created by Administrator on 2016/10/21.
 */

public class MyThread03 {

    class InnerObject {

        /**
         * 内部类方法01
         */
        private void innerMethod01() {
            for (int i = 0; i < 4; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * 内部类方法02
         */
        private void innerMethod02() {
            for (int i = 0; i < 8; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 外部方法01
     * @param innerObject 内部对象
     */
    protected void otherMethod01(InnerObject innerObject) {
        synchronized (innerObject) {
            innerObject.innerMethod01();
        }
    }

    /**
     * 外部方法02
     * @param innerObject 内部对象
     */
    protected void otherMethod02(InnerObject innerObject) {
        innerObject.innerMethod02();
    }
}
