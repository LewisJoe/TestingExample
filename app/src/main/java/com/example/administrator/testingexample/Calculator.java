package com.example.administrator.testingexample;

/**
 * Created by Administrator on 2016/10/19.
 */

public class Calculator {
    public double sum(double a, double b) {
        return a + b;
    }

    public double substract(double a, double b) {
        return a - b;
    }

    public double divide(double a, double b) {
        return a / b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }
}
